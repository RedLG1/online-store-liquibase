# Online Store Liquibase

## Относится к проекту
- [ ] [KTE Labs Test Task](https://gitlab.com/RedLG1/kte-labs)

## Команды для запуска приложения

### Все команды выполнять из корневого пакета проекта.
Сборка проекта:
```
mvn clean package
```
Для следующей команды должен быть запущен docker-compose.yaml файл из <br>[KTE Labs Test Task](https://gitlab.com/RedLG1/kte-labs).

Запуск приложения:
```
java -jar target/online-store-liquibase-0.0.1-SNAPSHOT.jar
```
После успешного запуска, приложение можно выключить.