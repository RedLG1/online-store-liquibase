package com.redl1.liquibase;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class OnlineStoreLiquibaseApplication {

	public static void main(String[] args) {
		SpringApplication.run(OnlineStoreLiquibaseApplication.class, args);
	}

}
