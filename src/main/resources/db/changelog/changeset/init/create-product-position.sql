--liquibase formatted sql

--changeset RedLG1:create-product-position
CREATE SEQUENCE product_position_sequence
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;

CREATE TABLE product_position (
                                  id bigint NOT NULL DEFAULT nextval('product_position_sequence'),
                                  count bigint NOT NULL,
                                  discount_price bigint NOT NULL,
                                  total_discount smallint NOT NULL,
                                  total_price bigint NOT NULL,
                                  product_id bigint NOT NULL,
                                  sale_fact_id bigint NOT NULL,
                                  CONSTRAINT product_position_pkey PRIMARY KEY (id),
                                  CONSTRAINT fk721ljj0m1cnnl2m9bq2gi1oiv FOREIGN KEY (product_id) REFERENCES product(id),
                                  CONSTRAINT fklmq295h0q4w7pq1hbiy27p0n2 FOREIGN KEY (sale_fact_id) REFERENCES sale_fact(id)
);

ALTER SEQUENCE product_position_sequence
    OWNED BY product_position.id;