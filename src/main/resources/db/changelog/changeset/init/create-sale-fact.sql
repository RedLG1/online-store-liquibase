--liquibase formatted sql

--changeset RedLG1:create-sale-fact
CREATE SEQUENCE sale_fact_sequence
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;

CREATE TABLE sale_fact (
                           id bigint NOT NULL DEFAULT nextval('sale_fact_sequence'),
                           receipt_number character varying(255),
                           "time" timestamp without time zone NOT NULL DEFAULT now(),
                           client_id bigint NOT NULL,
                           CONSTRAINT sale_fact_pkey PRIMARY KEY (id),
                           CONSTRAINT fk21r1ss0e96fdu4pdxnexpqxyj FOREIGN KEY (client_id) REFERENCES client(id),
                           CONSTRAINT idx_sale_fact_time_receipt_number UNIQUE ("time", receipt_number)
);

ALTER SEQUENCE sale_fact_sequence
    OWNED BY sale_fact.id;

CREATE INDEX idx_sale_fact_client_id_receipt_number ON sale_fact USING btree (client_id, receipt_number);