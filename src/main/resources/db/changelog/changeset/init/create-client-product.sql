--liquibase formatted sql

--changeset RedLG1:create-product
CREATE TABLE client_product (
                                client_id bigint NOT NULL,
                                product_id bigint NOT NULL,
                                grade smallint,
                                CONSTRAINT client_product_pkey PRIMARY KEY (client_id, product_id),
                                CONSTRAINT fkgqd72o2a9wa923a2s0757b5c0 FOREIGN KEY (client_id) REFERENCES client(id),
                                CONSTRAINT fkgromm4ojwcglhf5c70suhl3nj FOREIGN KEY (product_id) REFERENCES product(id)
);

CREATE INDEX idx_client_product_client_id_product_id ON client_product USING btree (client_id, product_id);
CREATE INDEX idx_client_product_product_id_grade ON client_product USING btree (product_id, grade);
