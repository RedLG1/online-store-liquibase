--liquibase formatted sql

--changeset RedLG1:create-client
CREATE SEQUENCE client_sequence
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;

CREATE TABLE client (
                        id bigint NOT NULL DEFAULT nextval('client_sequence'),
                        discount_1 smallint,
                        discount_2 smallint,
                        name character varying(255) NOT NULL,
                        CONSTRAINT client_pkey PRIMARY KEY (id)
);

ALTER SEQUENCE client_sequence
    OWNED BY client.id;