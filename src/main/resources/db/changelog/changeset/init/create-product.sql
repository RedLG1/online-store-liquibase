--liquibase formatted sql

--changeset RedLG1:create-product
CREATE SEQUENCE product_sequence
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;

CREATE TABLE product (
                         id bigint NOT NULL DEFAULT nextval('product_sequence'),
                         description character varying(255) NOT NULL,
                         discount smallint,
                         name character varying(255) NOT NULL,
                         price bigint NOT NULL,
                         CONSTRAINT product_pkey PRIMARY KEY(id)
);

ALTER SEQUENCE product_sequence
    OWNED BY product.id;