--liquibase formatted sql

--changeset RedLG1:insert-product
INSERT INTO product (id, description, name, price) VALUES
(1, 'Cool device', 'Laptop', 99999),
(2, 'Usual device', 'Smartphone', 27999),
(3, 'Cool device', 'PC', 199999);

SELECT setval('product_sequence', 3);