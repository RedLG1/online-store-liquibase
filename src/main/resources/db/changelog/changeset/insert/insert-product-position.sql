--liquibase formatted sql

--changeset RedLG1:insert-product-position
INSERT INTO product_position (id, count, discount_price, total_discount, total_price, product_id, sale_fact_id) VALUES
(2, 2, 197998, 1, 199998, 1, 1),
(3, 7, 192073, 2, 195993, 2, 1),
(5, 5, 449995, 10, 499995, 1, 2),
(6, 4, 106396, 5, 111996, 2, 2),
(7, 10, 1799991, 10, 1999990, 3, 2),
(9, 5, 474995, 5, 499995, 1, 3),
(10, 4, 106396, 5, 111996, 2, 3),
(11, 10, 1899990, 5, 1999990, 3, 3);

SELECT setval('product_position_sequence', 11);