--liquibase formatted sql

--changeset RedLG1:insert-client-product
INSERT INTO client_product (grade, client_id, product_id) VALUES
(1, 1, 1),
(2, 1, 2),
(3, 1, 3),
(5, 2, 1),
(5, 2, 2),
(null, 2, 3),
(4, 3, 1),
(3, 3, 2),
(2, 3, 3),
(4, 4, 1),
(3, 4, 2),
(2, 4, 3),
(4, 5, 1),
(3, 5, 2),
(2, 5, 3);