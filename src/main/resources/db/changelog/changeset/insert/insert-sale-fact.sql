--liquibase formatted sql

--changeset RedLG1:insert-sale-fact
INSERT INTO sale_fact (id, receipt_number, time, client_id) VALUES
(1, '00100', '2023-04-09 20:08:11.427007', 1),
(2, '00101', '2023-04-09 20:09:05.936954', 2),
(3, '00100', '2023-04-12 10:09:56.885434', 3);

SELECT setval('sale_fact_sequence', 3);