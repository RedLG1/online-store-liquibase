--liquibase formatted sql

--changeset RedLG1:insert-client
INSERT INTO client (id, discount_1, discount_2, name) VALUES
(1, 1, 2, 'Bob'),
(2, 5, 10, 'Carl'),
(3, 5, 5, 'Alex'),
(4, 4, 7, 'Jimmy'),
(5, 1, 2, 'Bob'),
(6, 5, 10, 'Carl'),
(7, 5, 5, 'Alex'),
(8, 4, 7, 'Jimmy'),
(9, 1, 2, 'Bob'),
(10, 5, 10, 'Carl'),
(11, 5, 5, 'Alex'),
(12, 4, 7, 'Jimmy'),
(13, 2, 10, 'Yaroslav');

SELECT setval('client_sequence', 13);